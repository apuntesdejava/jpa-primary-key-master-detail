/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.jpa.primary.key;

import com.apuntesdejava.jpa.primary.key.entities.Factura;
import com.apuntesdejava.jpa.primary.key.entities.FacturaDetalle;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author dsilva
 */
public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        new Main().start();

    }
    private final EntityManagerFactory emf;
    private final EntityManager em;

    <T> void persist(T obj) {

        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();

    }

    public Main() {
        emf = Persistence.createEntityManagerFactory("facturaPU");
        em = emf.createEntityManager();
    }

    private void start() {
        LOG.info("Insertando objeto factura");
        Factura factura = new Factura();
        factura.setFechaFactura(new Date());
        persist(factura);
        LOG.log(Level.INFO, "Factura insertada:{0}", factura);

        LOG.info("Insertando detalle 1");
        FacturaDetalle det1 = new FacturaDetalle(factura.getFacturaId(), 1);
        det1.setDescripcion("PCs");
        det1.setFactura(factura);
        persist(det1);
        LOG.log(Level.INFO, "detalle 1 insertada{0}", det1);

        LOG.info("Insertando detalle 2");
        FacturaDetalle det2 = new FacturaDetalle(factura.getFacturaId(), 2);
        det2.setDescripcion("Monitores");
        det2.setFactura(factura);
        persist(det2);
        LOG.log(Level.INFO, "detalle 2 insertada{0}", det2);

        LOG.info("Obteniendo objetos");
        List<Factura> facturas = em.createQuery("SELECT f FROM Factura f", Factura.class).getResultList();
        facturas.stream().forEach((f) -> {
            LOG.log(Level.INFO, "Factura: {0}", f);
        });

        close();

    }

    private void close() {
        em.close();
        emf.close();
    }
}
