/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.jpa.primary.key.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author dsilva
 */
@Embeddable
public class FacturaDetallePK implements Serializable {
    private long facturaId;
    private int orderId;

    public FacturaDetallePK() {
    }

    public FacturaDetallePK(long facturaId, int orderId) {
        this.facturaId = facturaId;
        this.orderId = orderId;
    }

    public long getFacturaId() {
        return facturaId;
    }

    public void setFacturaId(long facturaId) {
        this.facturaId = facturaId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (int) (this.facturaId ^ (this.facturaId >>> 32));
        hash = 59 * hash + this.orderId;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FacturaDetallePK other = (FacturaDetallePK) obj;
        if (this.facturaId != other.facturaId) {
            return false;
        }
        return this.orderId == other.orderId;
    }

    @Override
    public String toString() {
        return "FacturaDetallePK{" + "facturaId=" + facturaId + ", orderId=" + orderId + '}';
    }
    
}
