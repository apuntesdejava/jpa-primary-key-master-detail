/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.jpa.primary.key.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

/**
 *
 * @author dsilva
 */
@Entity
@Table(name = "FACTURA_DETALLE")
public class FacturaDetalle implements Serializable {

    @EmbeddedId
    private FacturaDetallePK facturaDetallePK;

    @MapsId("facturaId")
    @ManyToOne
    private Factura factura;

    private String descripcion;

    public FacturaDetalle() {
    }

    public FacturaDetalle(long facturaId, int ordenId) {
        facturaDetallePK = new FacturaDetallePK(facturaId, ordenId);
    }

    public FacturaDetallePK getFacturaDetallePK() {
        return facturaDetallePK;
    }

    public void setFacturaDetallePK(FacturaDetallePK facturaDetallePK) {
        this.facturaDetallePK = facturaDetallePK;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.facturaDetallePK);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final FacturaDetalle other = (FacturaDetalle) obj;
        return Objects.equals(this.facturaDetallePK, other.facturaDetallePK);
    }

    @Override
    public String toString() {
        return "FacturaDetalle{" + "facturaDetallePK=" + facturaDetallePK + ",  descripcion=" + descripcion + '}';
    }

}
